# Dynamic Quiz

Dynamic quiz is built using HTML, CSS and Pure JavaScript.

* get request
* fetch api
* class 
* static methods

Icons from [Freepik](https://www.flaticon.com/authors/freepik) and [Freepik](https://www.flaticon.com/authors/eucalyp)

Visit this page at [https://localhost:8080](https://localhost:8080)

## Requirements

Sass

## Installation

````bash
$ npm install
$ npm run start
$ npm run build:css
````