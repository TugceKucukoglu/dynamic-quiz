class Request {
  constructor() {
    this.json = '../quiz.json';
    // this.json = './questions.js';
  }
  getJSON(json) {
    return new Promise((resolve, reject) => {
      fetch(json)
        .then(response => response.json())
        .then(data => resolve(data))
        .catch(err => reject(err))
    });
  }
}