 const start = document.querySelector('.header__start');
 const header = document.querySelector('.header');
 const quiz = document.querySelector('.quiz');
 const correctAnswers = ['B', 'D', 'C', 'A'];

 const request = new Request();

 let jsonArray;
 let slideNumber = 0;
 let textData;

 eventListener();

 function eventListener() {
   window.addEventListener('load', jsonData);
   start.addEventListener('click', quizStart);
   // quizAnswer.addEventListener('click', takeAnswer);
 }

 function jsonData() {
   request.getJSON(request.json)
     .then(data => {
       // console.log(data);
       // textData = JSON.stringify(data);
       textData = data;
       // console.log(textData);
     })
     .catch(err => console.error(err))
     .finally();
 }

 function quizStart() {
   console.log('slideNumber: ' + slideNumber);
   if (quiz.className.includes('show')) {
     quiz.classList.replace('show', 'hide');
     header.classList.replace('hide', 'show');
   }
   if (quiz.className.includes('hide')) {
     header.classList.replace('show', 'hide');
     quiz.classList.replace('hide', 'show');
   }

   showQuestion(slideNumber);
 }

 function showQuestion(slideNumber) {
   // textData = JSON.parse(textData);
   if (slideNumber == textData.length) {
     slideNumber = 0;
     const answer = new Answer();
     let selectedAnswers = answer.sendAnswer();
     checkAnswer(selectedAnswers);
   } else {
     console.log('slideNumber: ' + slideNumber);
     let question = textData[slideNumber]['question'];
     let img = textData[slideNumber]['img'];
     let select1 = textData[slideNumber]['select1'];
     let select2 = textData[slideNumber]['select2'];
     let select3 = textData[slideNumber]['select3'];
     let select4 = textData[slideNumber]['select4'];

     const slide = new Slide(slideNumber, question, img, select1, select2, select3, select4);

     UI.addUI(slide);
     UI.takeAnswer(slideNumber, textData.length);

   }
 }

 function checkAnswer(selectedAnswers) {
   console.log(selectedAnswers);
   let correct = 0;
   selectedAnswers.forEach((el, index) => {
     if (el == correctAnswers[index]) {
       correct++;
     }
     UI.showResult(correct);
   });
 }