class Slide {
  constructor(slideNumber, question, img, sel1, sel2, sel3, sel4) {
    this.slideNumber = slideNumber;
    this.question = question;
    this.img = img;
    this.select1 = sel1;
    this.select2 = sel2;
    this.select3 = sel3;
    this.select4 = sel4;
  }
  show() {
    console.log(this.question);
  }
}