const questions = [{
  "question": "Who did prove that in a right-angle triangle, the squares of two sides are equal to the square of the hypotenuse, i.e. a<sup>2</sup> + b<sup>2</sup> = h<sup>2</sup> ?",
  "img": "./img/trigonometry.png",
  "select1": "Euclid",
  "select2": "Pythagoras",
  "select3": "Aristotle",
  "select4": "Diophantus",
  "answer": "B"
}, {
  "question": "Who did the first mathematician define the 'point' and 'line' and then gave the five postulates?",
  "img": "./img/axis1.png",
  "select1": "Alan Turing",
  "select2": "George Riemann",
  "select3": "Rene Descartes",
  "select4": "Euclid",
  "answer": "D"
}, {
  "question": "Who did help to establish modern symbolic logic and whose algebra of logic, is basic to the design of digital computer circuits.",
  "img": "./img/boolean.png",
  "select1": "George Riemann",
  "select2": "Rene Descartes",
  "select3": "George Boole",
  "select4": "Evariste Galois",
  "answer": "C"
}, {
  "question": "Who did devise the first systematic method for breaking messages encrypted by the sophisticated German cipher machine that the British called 'Tunny.'",
  "img": "./img/enigma3.jpg",
  "select1": "Alan Turing",
  "select2": "George Boole",
  "select3": "George Cantor",
  "select4": "Kurt Gödels",
  "answer": "A"
}]
export default questions;