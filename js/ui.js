class UI {
  static addUI(slide) {
    const row = document.querySelector('.quiz__row');
    let slideNumber = slide.slideNumber;
    console.log('slideNumber: ' + slideNumber);
    row.innerHTML = `
    <div class="quiz__container">
      <div class="quiz__question">${slide.question}</div>
        <ul class="quiz__answer">
          <li class="quiz__answer--select" data-value="A"><span class="quiz__span">A</span>${slide.select1}</li>
          <li class="quiz__answer--select" data-value="B"><span class="quiz__span">B</span>${slide.select2}</li>
          <li class="quiz__answer--select" data-value="C"><span class="quiz__span">C</span>${slide.select3}</li>
          <li class="quiz__answer--select" data-value="D"><span class="quiz__span">D</span>${slide.select4}</li>
        </ul>
      </div>
    <div class="quiz__img--container"><img class="quiz__img" src="${slide.img}" alt="" /></div>
    `;
    const container = document.querySelector('.quiz__container');
    if (slideNumber == 3) {
      container.innerHTML += `<span class="quiz__next-page">Show Result</span>`
    } else {
      container.innerHTML += `<span class="quiz__next-page">Next Question</span>`
    }
  }

  static takeAnswer(slideNumber, textDataLength) {
    const quizAnswer = document.querySelector('.quiz__answer');
    const quizSelections = document.querySelectorAll('.quiz__answer--select');
    const nextPage = document.querySelector('.quiz__next-page');
    let x;
    quizAnswer.addEventListener('click', e => {
      // let x;
      quizSelections.forEach(select => {
        if (e.target == select || e.target.parentElement == select) {
          if (e.target.tagName == 'SPAN') {
            x = e.target.parentElement.dataset.value;
            e.target.parentElement.classList.add('bg-green');
          }
          if (e.target.tagName == 'LI') {
            x = e.target.dataset.value;
            e.target.classList.add('bg-green');
          }
        } else {
          select.classList.remove('bg-green');
          select.firstElementChild.classList.remove('bg-green');
        }
      });
    });

    nextPage.addEventListener('click', () => {
      console.log(x);
      if (x != undefined) {
        const answer = new Answer(x);
        answer.showVal(textDataLength);
        slideNumber++;
        console.log('takeAnswer => slideNumber: ' + slideNumber);
        showQuestion(slideNumber);
        // return slideNumber;
        // setTimeout(showQuestion(slideNumber), 3000);
      } else {
        const container = document.querySelector('.quiz__container');
        const warning = document.createElement('div');
        warning.setAttribute('class', 'bg-red');
        warning.innerText = 'Please make a choice.';
        container.appendChild(warning);
        setTimeout(() => {
          container.removeChild(warning);
        }, 2000);
      }
    });
    // return -1;
  }

  static showResult(correct) {
    const row = document.querySelector('.quiz__row');
    row.innerHTML = `
    <div class="quiz__container">
      <div class="quiz__result"></div>
    </div>
    <div class="quiz__img--container"><img class="quiz__img" src="../img/math1.png" alt="" /></div>
    `;
    const quizResult = document.querySelector('.quiz__result');
    let output = 0;
    let result = correct * 25;
    const timer = setInterval(() => {
      quizResult.innerHTML = `Your score is % ${output}`;
      if (output == result) clearInterval(timer);
      else output++;
    }, 10);
  }
}